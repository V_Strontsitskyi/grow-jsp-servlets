package com.epam.strontsitskyi.dao;

import com.epam.strontsitskyi.model.Address;

/**
 *
 * @author Volodymyr Strontsitskyi
 */
public interface AddressDAO extends CommonDAO<Address>
{
}
