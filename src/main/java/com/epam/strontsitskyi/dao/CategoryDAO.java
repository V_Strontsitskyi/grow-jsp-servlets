package com.epam.strontsitskyi.dao;

import com.epam.strontsitskyi.model.Category;

/**
 * 
 * @author Volodymyr Strontsitskyi
 */
public interface CategoryDAO extends CommonDAO<Category>
{
    Category getByName(String name);
}
