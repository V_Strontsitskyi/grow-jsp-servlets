package com.epam.strontsitskyi.dao;

import com.epam.strontsitskyi.model.City;

/**
 * 
 * @author Volodymyr Strontsitskyi
 */
public interface CityDAO extends CommonDAO<City>
{
}
