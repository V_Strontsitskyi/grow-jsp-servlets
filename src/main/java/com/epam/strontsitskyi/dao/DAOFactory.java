package com.epam.strontsitskyi.dao;

import com.epam.strontsitskyi.dao.impl.*;
import com.epam.strontsitskyi.model.*;

public class DAOFactory
{
    private static DAOFactory instance = new DAOFactory();

    private AddressDAO addressDAO;
    private CategoryDAO categoryDAO;
    private CityDAO cityDAO;
    private MeasurementUnitDAO measurementUnitDAO;
    private ProductDAO productDAO;
    private SupplierDAO supplierDAO;
    private UserDAO userDAO;

    private DAOFactory()
    {
    }

    public static DAOFactory getInstance()
    {
        return instance;
    }

    public AddressDAO getAddressDAO()
    {
        if (addressDAO==null)
            addressDAO= new AddressDAOImpl(Address.class);
        return addressDAO;
    }

    public CategoryDAO getCategoryDAO()
    {
        if (categoryDAO==null)
            categoryDAO= new CategoryDAOImpl(Category.class);
        return categoryDAO;
    }

    public CityDAO getCityDAO()
    {
        if (cityDAO == null)
            cityDAO = new CityDAOImpl(City.class);
        return cityDAO;
    }

    public MeasurementUnitDAO getMeasurementUnitDAO()
    {
        if (measurementUnitDAO == null)
            measurementUnitDAO = new MeasurementUnitDAOImpl(MeasurementUnit.class);
        return measurementUnitDAO;
    }

    public ProductDAO getProductDAO()
    {
        if (productDAO == null)
            productDAO = new ProductDAOImpl(Product.class);
        return productDAO;
    }

    public SupplierDAO getSupplierDAO()
    {
        if(supplierDAO==null)
            supplierDAO= new SupplierDAOImpl(Supplier.class);
        return supplierDAO;
    }

    public UserDAO getUserDAO()
    {
        if(userDAO==null)
            userDAO= new UserDAOImpl(User.class);
        return userDAO;
    }
}
