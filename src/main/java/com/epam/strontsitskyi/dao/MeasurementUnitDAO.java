package com.epam.strontsitskyi.dao;

import com.epam.strontsitskyi.model.MeasurementUnit;

/**
 *
 * @author Volodymyr Strontsitskyi
 */
public interface MeasurementUnitDAO extends CommonDAO<MeasurementUnit>
{
}
