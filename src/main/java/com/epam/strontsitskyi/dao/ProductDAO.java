package com.epam.strontsitskyi.dao;

import com.epam.strontsitskyi.model.Product;

/**
 *
 * @author Volodymyr Strontsitskyi
 */
public interface ProductDAO extends CommonDAO<Product>
{
}
