package com.epam.strontsitskyi.dao;

import com.epam.strontsitskyi.model.Supplier;

/**
 *
 * @author Volodymyr Strontsitskyi
 */
public interface SupplierDAO extends CommonDAO<Supplier>
{
}
