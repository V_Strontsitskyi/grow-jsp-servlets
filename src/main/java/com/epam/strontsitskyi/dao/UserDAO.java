package com.epam.strontsitskyi.dao;

import com.epam.strontsitskyi.model.User;

/**
 * 
 * @author Volodymyr Strontsitskyi
 */
public interface UserDAO extends CommonDAO<User>
{
    User getByEmail(String email);
}
