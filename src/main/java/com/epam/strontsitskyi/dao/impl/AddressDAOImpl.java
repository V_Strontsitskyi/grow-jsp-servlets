package com.epam.strontsitskyi.dao.impl;

import com.epam.strontsitskyi.dao.AddressDAO;
import com.epam.strontsitskyi.model.Address;

/**
 * 
 * @author Volodymyr Strontsitskyi
 */
public class AddressDAOImpl extends CommonDAOImpl<Address> implements AddressDAO
{

    public AddressDAOImpl(Class<Address> clazz)
    {
        super(clazz);
    }
}
