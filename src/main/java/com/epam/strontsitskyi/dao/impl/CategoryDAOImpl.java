package com.epam.strontsitskyi.dao.impl;

import org.hibernate.Session;

import com.epam.strontsitskyi.dao.CategoryDAO;
import com.epam.strontsitskyi.model.Category;

/**
 * 
 * @author Volodymyr Strontsitskyi
 */
public class CategoryDAOImpl extends CommonDAOImpl<Category> implements CategoryDAO
{
    public CategoryDAOImpl(Class<Category> clazz)
    {
        super(clazz);
    }

    @Override
    public Category getByName(String name)
    {
        Category result = null;
        Session session = null;
        try
        {
            session = acquireSession();
            result = (Category) session.createQuery("from  Category where name = :name ").setParameter("name", name).uniqueResult();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            closeSession(session);
        }
        return result;
    }
}
