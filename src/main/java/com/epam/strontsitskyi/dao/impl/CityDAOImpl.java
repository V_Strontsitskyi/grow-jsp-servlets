package com.epam.strontsitskyi.dao.impl;

import com.epam.strontsitskyi.dao.CityDAO;
import com.epam.strontsitskyi.model.City;

/**
 * @author Volodymyr Strontsitskyi
 */
public class CityDAOImpl extends CommonDAOImpl<City> implements CityDAO
{

    public CityDAOImpl(Class<City> clazz)
    {
        super(clazz);
    }
}
