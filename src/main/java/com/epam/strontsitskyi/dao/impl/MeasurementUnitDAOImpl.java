package com.epam.strontsitskyi.dao.impl;

import com.epam.strontsitskyi.dao.MeasurementUnitDAO;
import com.epam.strontsitskyi.model.MeasurementUnit;

/**
 * 
 * @author Volodymyr Strontsitskyi
 */
public class MeasurementUnitDAOImpl extends CommonDAOImpl<MeasurementUnit> implements MeasurementUnitDAO
{
    public MeasurementUnitDAOImpl(Class<MeasurementUnit> clazz)
    {
        super(clazz);
    }
}
