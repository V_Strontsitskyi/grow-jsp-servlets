package com.epam.strontsitskyi.dao.impl;

import com.epam.strontsitskyi.dao.ProductDAO;
import com.epam.strontsitskyi.model.Product;

/**
 *
 * @author Volodymyr Strontsitskyi
 */
public class ProductDAOImpl extends CommonDAOImpl<Product> implements ProductDAO
{
    public ProductDAOImpl(Class<Product> clazz)
    {
        super(clazz);
    }
}
