package com.epam.strontsitskyi.dao.impl;

import com.epam.strontsitskyi.dao.SupplierDAO;
import com.epam.strontsitskyi.model.Supplier;

/**
 * 
 * @author Volodymyr Strontsitskyi
 */
public class SupplierDAOImpl extends CommonDAOImpl<Supplier> implements SupplierDAO
{
    public SupplierDAOImpl(Class<Supplier> clazz)
    {
        super(clazz);
    }
}
