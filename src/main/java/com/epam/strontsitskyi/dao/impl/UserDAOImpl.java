package com.epam.strontsitskyi.dao.impl;

import org.hibernate.Session;

import com.epam.strontsitskyi.dao.UserDAO;
import com.epam.strontsitskyi.model.User;

/**
 *
 * @author Volodymyr Strontsitskyi
 */
public class UserDAOImpl extends CommonDAOImpl<User> implements UserDAO
{
    public UserDAOImpl(Class<User> clazz)
    {
        super(clazz);
    }

    @Override
    public User getByEmail(String email)
    {
        User result = null;
        Session session = null;
        try
        {
            session = acquireSession();
            result = (User) session.createQuery("from  User where email = :email ").setParameter("email", email).uniqueResult();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            closeSession(session);
        }
        return result;
    }
}
