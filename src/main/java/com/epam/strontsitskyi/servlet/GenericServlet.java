package com.epam.strontsitskyi.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.strontsitskyi.dao.CategoryDAO;
import com.epam.strontsitskyi.dao.DAOFactory;
import com.epam.strontsitskyi.model.Category;

/**
 * 
 * @author Volodymyr Strontsitskyi
 */
public class GenericServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        req.getRequestDispatcher("generic.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        if (req.getParameter("_method") == null)
        {
            DAOFactory daoFactory = DAOFactory.getInstance();
            CategoryDAO categoryDAO = daoFactory.getCategoryDAO();
            Category category = new Category();
            category.setName(req.getParameter("demo-name"));
            categoryDAO.add(category);
            req.getRequestDispatcher("/grow/generic").forward(req, resp);

        }
        else if (req.getParameter("_method").equals("delete"))
        {
            doDelete(req, resp);
        }
        else if (req.getParameter("_method").equals("put"))
        {
            doPut(req, resp);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        DAOFactory daoFactory = DAOFactory.getInstance();
        CategoryDAO categoryDAO = daoFactory.getCategoryDAO();
        Category category = categoryDAO.getByName(req.getParameter("demo-name"));
        categoryDAO.remove(category);
        req.getRequestDispatcher("/grow/generic").forward(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        DAOFactory daoFactory = DAOFactory.getInstance();
        CategoryDAO categoryDAO = daoFactory.getCategoryDAO();
        Category category = categoryDAO.getByName(req.getParameter("demo-name"));
        category.setName(req.getParameter("new-name"));
        categoryDAO.update(category);
        req.getRequestDispatcher("/grow/generic").forward(req, resp);
    }
}
