package com.epam.strontsitskyi.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.strontsitskyi.dao.DAOFactory;
import com.epam.strontsitskyi.dao.UserDAO;
import com.epam.strontsitskyi.model.User;
import com.epam.strontsitskyi.util.MD5Util;

/**
 *
 * @author Volodymyr Strontsitskyi
 */
public class LogInServlet extends HttpServlet
{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        DAOFactory daoFactory = DAOFactory.getInstance();
        UserDAO userDao = daoFactory.getUserDAO();
        User user = userDao.getByEmail(request.getParameter("email"));
        HttpSession session = request.getSession();
        if (user.getPassword().equals(MD5Util.md5Apache(request.getParameter("password"))))
        {
            session.setAttribute("user", user);
        }
        response.sendRedirect((String) session.getAttribute("currentpagesign"));
    }

}
