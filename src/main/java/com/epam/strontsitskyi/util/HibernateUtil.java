package com.epam.strontsitskyi.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Volodymyr Strontsitskyi
 */
public class HibernateUtil
{
    private static SessionFactory sessionFactory;

    private HibernateUtil()
    {
    }

    static
    {
        sessionFactory = configureSessionFactory();
    }

    public static Session getSession()
    {
        return sessionFactory.openSession();
    }

    private static SessionFactory configureSessionFactory() throws HibernateException
    {
        Configuration configuration = new Configuration().configure();
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        sessionFactory = configuration.buildSessionFactory(builder.build());
        return sessionFactory;
    }
}
