package com.epam.strontsitskyi.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Volodymyr Strontsitskyi
 */
public class MD5Util
{
    public static String md5Apache(String st)
    {
        String md5Hex = DigestUtils.md5Hex(st);
        return md5Hex;
    }

}
